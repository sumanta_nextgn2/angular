angular.module('ux2', []);

angular.module('ux2')
    .directive('ux2MultiSelect', ["$timeout", "$document", "$filter", function ($timeout, $document, $filter) {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'lib/ux/tpl/ux2.multiselect.tpl.html',
            scope: {
                selectedItems: "=",
                listData: "=",
                itemProperty: "@",
                widthAdjust: "@"
            },
            link: function (scope, el) {

                var element = el[0];

                function setMultSelectResultPostion() {
                    var adj = 0;
                    if (scope.widthAdjust) {
                        adj = scope.widthAdjust;
                    }

                    scope.width = $(element).width() - 17 + parseInt(adj);
                    scope.top = $(element).position().top + 28;
                    scope.leftToolTip = $(element).position().left;


                    $(element).find(".mult-select-result").css("top", scope.top + "px");
                    $(element).find(".mult-select-result").css("width", scope.width + "px");

                    scope.topToolTip = scope.top;
                    if (scope.selectedItems) {
                        //scope.topToolTip = scope.topToolTip - (scope.selectedItems.length * 20);
                    }
                }

                function toggleMultSelectResult() {
                    if ($(element).find(".mult-select-result").hasClass("ng-hide")) {
                        $(element).find(".mult-select-result").removeClass("ng-hide");
                    } else {
                        $(element).find(".mult-select-result").addClass("ng-hide");
                    }
                }

                function hideMultSelectResult() {
                    if (!$(element).find(".mult-select-result").hasClass("ng-hide")) {
                        $(element).find(".mult-select-result").addClass("ng-hide");
                    }
                }

                function displaydata() {
                    if (scope.selectedItems && scope.selectedItems.length >= 1) {
                        var textToDisplay = scope.selectedItems[0][scope.itemProperty];
                        if (textToDisplay.length > maxTextToDisplay) {
                            scope.displayData = textToDisplay.substring(0, maxTextToDisplay) + ".. ";
                        } else {
                            scope.displayData = textToDisplay + " ";
                        }

                    } else {
                        scope.displayData = "";
                    }
                }

                setMultSelectResultPostion();

                var filteredData = [];
                var maxTextToDisplay = 12;


                scope.showInfo = function () {
                    setMultSelectResultPostion();
                    scope.showToolTip = true;
                }

                scope.selectItem = function (item) {
                    if (item.selected) {
                        item.selected = false;
                    } else {
                        item.selected = true;
                    }

                    filteredData = $filter('filter')(scope.listData, function (i) {
                        return i.selected == true;
                    });

                    scope.selectedItems = filteredData;

                    displaydata();
                }

                scope.showSection = function () {
                    setMultSelectResultPostion();
                    // Scope vriable wont work due to uxScroll in transcluded scope
                    toggleMultSelectResult();
                }

                scope.$watch("selectedItems", function (newVal, oldVal) {
                    if (newVal) {
                        angular.forEach(scope.selectedItems, function (i, key) {
                            i.selected = true;
                        });
                        displaydata();
                    }
                });

                $document.on("click", function (event) {
                    if (event.target.classList.contains("mult-select-selector")) {
                        return;
                    }

                    if (event.target.classList.length == 0 || !event.target.classList.contains("mult-select-selector-primary")) {
                        hideMultSelectResult();
                        return;
                    }
                });
            }
        };
    }]);

angular.module('ux2')
    .directive('ux2AutoComplete', ["$timeout", function ($timeout) {

        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'lib/ux/tpl/ux2.autocomplete.tpl.html',
            scope: {
                updateFn: '&',
                selectedItem: "=",
                itemProperty: "@",
                minLength: "@"
            },
            link: function (scope, el) {

                var element = el[0];

                var timer;
                var currentPage = 1;
                var oldPageNumber = currentPage;
                var oldSearchtext = "";


                function resetAutoCompleteData() {
                    currentPage = 1;
                    scope.data = [];
                }

                resetAutoCompleteData();

                //var element = document.getElementById("auto-container");

                function loadMoreItems() {
                    var scrollElement = $(element).find(".auto-container");
                    if (scrollElement.scrollTop() + scrollElement.innerHeight() >= scrollElement[0].scrollHeight) {

                        if (oldPageNumber == currentPage + 1) {
                            console.log("exiting load function as old page is ", oldPageNumber + " and new page is ", currentPage + 1);
                            return;
                        }

                        console.log("calling load function as old page is ", oldPageNumber, " and new page is ", currentPage + 1);

                        oldPageNumber = currentPage + 1;

                        scope.updateFn()(scope.searchText, currentPage + 1)
                            .then(function (result) {
                                if (result.length > 0) {
                                    scope.data = scope.data.concat(result);
                                    scope.maxIndex = scope.data.length - 1;
                                    currentPage = currentPage + 1;
                                    oldPageNumber = currentPage;
                                    oldSearchtext = scope.searchText;
                                }
                            });
                    }
                }

                scope.search = function () {

                    if (timer) {
                        $timeout.cancel(timer);
                    }
                    timer = $timeout(function () {

                        if (scope.searchText.length == 0) {
                            resetAutoCompleteData();
                            return;
                        }

                        if (scope.searchText.length < scope.minLength) {
                            return;
                        }

                        scope.top = $(element).position().top + 30 + "px";
                        scope.width = $(element).width() + "px";
                        currentPage = 1;

                        console.log(scope.searchText, currentPage);

                        scope.updateFn()(scope.searchText, currentPage)
                            .then(function (result) {
                                scope.data = result;
                                scope.currentSelectedIndex = -1;
                                scope.maxIndex = scope.data.length - 1;
                                scope.showAutoSection = true;
                                oldPageNumber = currentPage;
                                oldSearchtext = scope.searchText;
                                //$(".auto-container").css('top', $(element).find(".auto-search-text-input").position().top + 30 + "px");
                            });
                    }, 500);
                };

                var scrollEvent = true;
                var dataLessThanScroll = true;

                scope.mousedown = function (e) {
                    if (e.target.classList.contains("auto-container")) {
                        console.log(scope.data.length);

                        scrollEvent = false;

                        if (scope.data.length <= 7) {
                            dataLessThanScroll = true;
                        } else {
                            dataLessThanScroll = false;
                        }
                        $(element).find(".auto-search-text-input").focus();
                    } else {
                        scrollEvent = true;
                        $(element).find(".auto-search-text-input").focus();
                    }
                };

                scope.onBlur = function (e) {
                    if (dataLessThanScroll) {
                        scope.showAutoSection = false;
                        return;
                    }

                    if (scrollEvent) {
                        scope.showAutoSection = false;
                    }
                };

                scope.key = function ($event) {
                    if ($event.keyCode == 38) { // key up
                        if (scope.currentSelectedIndex > 0) {
                            scope.currentSelectedIndex = scope.currentSelectedIndex - 1;
                            if ((scope.maxIndex - scope.currentSelectedIndex) > 5) {
                                element.scrollTop -= 30;
                            }
                            return;
                        }
                    } else if ($event.keyCode == 40) { // key down
                        if (scope.currentSelectedIndex < scope.maxIndex) {
                            scope.currentSelectedIndex = scope.currentSelectedIndex + 1;

                            if (scope.currentSelectedIndex > 5) {
                                element.scrollTop += 30;
                            }
                        }

                        if (scope.currentSelectedIndex == scope.maxIndex - 1) {
                            loadMoreItems();
                        }
                    } else if ($event.keyCode == 13) { // enter
                        scope.selectItem(scope.data[scope.currentSelectedIndex]);
                        return;
                    } else if ($event.keyCode == 27) { // escape
                        scope.showAutoSection = false;
                        return;
                    }
                };

                scope.selectItem = function (item) {
                    scope.selectedItem = item;
                    scope.searchText = scope.selectedItem[scope.itemProperty];
                    scope.showAutoSection = false;
                    scope.data = []; // to Reduce dom loading
                };

                $(element).find(".auto-container").bind('mousewheel DOMMouseScroll', function (e) {
                    var scrollTo = null;

                    if (e.type == 'mousewheel') {
                        scrollTo = (e.originalEvent.wheelDelta * -1);
                        loadMoreItems();
                    } else if (e.type == 'DOMMouseScroll') {
                        scrollTo = 40 * e.originalEvent.detail;
                        loadMoreItems();
                    }

                    if (scrollTo) {
                        e.preventDefault();
                        $(this).scrollTop(scrollTo + $(this).scrollTop());
                    }

                    $(element).find(".auto-search-text-input").focus();
                    scrollEvent = true;
                });

                $(element).find(".auto-container").scroll(function (e) {
                    loadMoreItems();
                    e.preventDefault();

                    $(element).find(".auto-search-text-input").focus();
                    scrollEvent = true;
                });
            }
        };
    }]);

angular.module("ux2").directive("ux2CustomTemplate", ["$compile", function ($compile) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            itemDisplayProperty: "=",
            itemDisplay: "=",
            itemObject: "="
        },
        link: function (scope, element) {
            var str = "";

            if (scope.itemDisplayProperty && scope.itemDisplayProperty != null) {
                str = "<span style='padding-right: 2px;'>{{itemObject[itemDisplayProperty] }}</span>";
            } else {
                var displayText = scope.itemDisplay.replace(/\\/gi, "");
                displayText = displayText.replace(/{{/gi, "{{itemObject.");
                str = "<span style='padding-right: 2px;'>" + displayText + "</span>";
            }

            element.html(str);
            $compile(element.contents())(scope);
        }
    }
}]);

angular.module('ux2')
    .directive('ux2SingleSelect', ["$timeout", "$document", "$filter", function ($timeout, $document, $filter) {
        return {
            restrict: 'EA',
            replace: true,
            templateUrl: 'lib/ux/tpl/ux2.singleselect.tpl.html',
            scope: {
                itemList: "&",
                itemSelectedValueProperty: "@",
                itemSelectedValue: "=",
                itemDisplayProperty: "@",
                itemDisplay: "@",
                itemSelected: "=ngModel",
                allowBlank: "=",
                blankLabel: "@",
                widthAdjust: "@",
                onChange: "&"
            },
            link: function (scope, el) {

                var element = el[0];

                if (!scope.widthAdjust) {
                    console.log("width adjustment");
                    scope.widthAdjust = 17;
                }

                function setMultSelectResultPostion() {
                    var adj = 0;
                    if (scope.widthAdjust) {
                        adj = scope.widthAdjust;
                    }

                    scope.width = $(element).width() - 17 + parseInt(adj);
                    scope.top = $(element).position().top + 28;
                    scope.leftToolTip = $(element).position().left;

                    $(element).find(".mult-select-result").css("top", scope.top + "px");
                    $(element).find(".mult-select-result").css("width", scope.width + "px");
                }

                function toggleMultSelectResult() {
                    if ($(element).find(".mult-select-result").hasClass("ng-hide")) {
                        $(element).find(".mult-select-result").removeClass("ng-hide");
                    } else {
                        $(element).find(".mult-select-result").addClass("ng-hide");
                    }
                }

                function hideMultSelectResult() {
                    if (!$(element).find(".mult-select-result").hasClass("ng-hide")) {
                        $(element).find(".mult-select-result").addClass("ng-hide");
                    }
                }

                function displaydata() {
                    scope.itemSelected = $filter('filter')(scope.itemList(), function (i) {
                        return i[scope.itemSelectedValueProperty] == scope.itemSelectedValue;
                    })[0];
                }

                setMultSelectResultPostion();

                var filteredData = [];
                var maxTextToDisplay = 12;
                scope.showDropDown = false;

                scope.selectItem = function (item) {
                    scope.showBlank = false;
                    angular.forEach(scope.listData, function (i, key) {
                        i.selected = false;
                    });

                    item.selected = true;
                    scope.itemSelected = item;
                    scope.showDropDown = false;
                    scope.onChange()(scope.itemSelected);
                    //hideMultSelectResult();
                }

                scope.selectBlank = function () {
                    scope.showBlank = true;
                    scope.showDropDown = false;
                    scope.itemSelected = null;
                    scope.onChange()(scope.itemSelected);
                }

                scope.showSection = function () {
                    setMultSelectResultPostion();
                    scope.showDropDown = !scope.showDropDown;

                    // Scope vriable wont work due to uxScroll in transcluded scope
                    //toggleMultSelectResult();
                }

                scope.$watch("itemSelectedValue", function (newVal, oldVal) {
                    if (newVal) {
                        displaydata();
                    }
                });

                $document.on("click", function (event) {
                    if (event.target.classList.contains("mult-select-selector")) {
                        return;
                    }

                    if (event.target.classList.length == 0 || !event.target.classList.contains("mult-select-selector-primary")) {
                        hideMultSelectResult();
                        return;
                    }
                });
            }
        };
    }]);

angular.module('ux2').directive('bindHtmlCompile', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.bindHtmlCompile);
            }, function (value) {
                // In case value is a TrustedValueHolderType, sometimes it
                // needs to be explicitly called into a string in order to
                // get the HTML string.
                element.html(value && value.toString());
                // If scope is provided use it, otherwise use parent scope
                var compileScope = scope;
                if (attrs.bindHtmlScope) {
                    compileScope = scope.$eval(attrs.bindHtmlScope);
                }
                $compile(element.contents())(compileScope);
            });
        }
    };
}]);

if (!String.prototype.startsWith) {
    String.prototype.startsWith = function (searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
    };
}